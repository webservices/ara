# ARA: Ansible Run Analysis

- More info about the project can be retrieved on [Webservices Internals](https://webservices-internals.web.cern.ch/webservices-internals/)
- [Original source code](https://github.com/openstack/ara)
- [Official documentation](https://ara.readthedocs.io/en/latest/index.html)

### To be removed
Deploy:

```oc new-app centos/python-27-centos7~https://gitlab.cern.ch/webservices/ara.git -e APP_HOME=./ara \																						  
-e ARA_DATABASE "postgresql+psycopg2://<user>:<password>@<host>:<port>/<database_name>" \
-e ANSIBLE_CALLBACK_PLUGINS=./ara/plugins/callbacks \
-e ANSIBLE_ACTION_PLUGINS=./ara/plugins/actions \
-e ANSIBLE_LIBRARY=./ara/plugins/modules \```
